<?php

require('constant.php');
require('database.php');

if (!isset($_GET['client'])) {
	die();
}else{
    $clientName = $_GET['client'];
    $clientData = obtainClientInfo($clientName);
    echo json_encode($clientData);
}

?>