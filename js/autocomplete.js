var MIN_LENGTH = 3;


function clearInfo(){
    document.getElementById('keyword').readOnly= false;
    document.getElementById('keyword').value= "";
    $('#selectedClientInformation').hide("slow");
    $("#keyword").keyup();
    document.getElementById('keyword').focus();
    
    
}


$( document ).ready(function() {

    $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
    });
    
	$("#keyword").keyup(function() {
		var keyword = $("#keyword").val();
		if (keyword.length >= MIN_LENGTH) {
            console.log(keyword);
			$.get( "autocomplete.php", { keyword: keyword } )
			.done(function( data ) {
                console.log(data);
				$('#results').html('');
				var results = jQuery.parseJSON(data);
				var numberOfResults =0;
                $(results).each(function(key, value) {
                    console.log(value);
                    if (numberOfResults < 3){
					   $('#results').append('<div class="item">' + value + '</div>');
                        numberOfResults += 1;
                    }
				})
                numberOfResults=0;

			    
                
                //Por alguna razón cuando se hace un fetch de los elmentos de una fila no puedo regresarlos inherentamente como un arreglo
                //sino que solamente los regresa como objeto. Por eso se utiliza var array= $.map para pasar los objetos a un arreglo
                
                $('.item').click(function() {
			    	var client = $(this).html();
                    console.log(client);
                    
			    	$('#keyword').val(client);
                    $('#selectedClientInformation').show("slow");
                    $('#assignButton').show("slow");
                    document.getElementById('keyword').readOnly= true;
                    
                    $.get("autocompleteInfo.php",  {  client: client } )
                    .done(function (clientData) {
                        console.log(clientData);
                        var information = jQuery.parseJSON(clientData);
                        var output = '';
                        var array = $.map(information, function(value, index){
                            return [value];
                        });
                        console.log(array);
                        $('#rfc').val(array[0]);
                        $('#email').val(array[1]);
                        $('#phone').val(array[2]);
                    });
			    });
                
                
                
                
			});
		} else {
			$('#results').html('');
            $('#selectedClientInformation').hide("slow");
            $('#assignButton').hide("slow");
            
		}
	});

    $("#keyword").blur(function(){
    		$("#results").fadeOut(500);
    	})
        .focus(function() {		
    	    $("#results").show();
    	});

});
