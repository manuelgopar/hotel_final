<?php

require('constant.php');
require('database.php');

if (isset($_GET['ready'])) {
    $keyword = $_GET['ready'];
    $data = searchOccupiedRooms();
    echo json_encode($data);
}
if (isset($_GET['number'])) {
	$number = $_GET['number'];
    $clientData = searchCheckoutData($number);
    echo json_encode($clientData);
}
?>