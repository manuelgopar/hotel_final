-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2015 at 02:24 AM
-- Server version: 5.6.26
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hoteldatabase`
--

-- --------------------------------------------------------

--
-- Table structure for table `habitaciones`
--

CREATE TABLE IF NOT EXISTS `habitaciones` (
  `numero` int(11) NOT NULL,
  `tipo` varchar(10) NOT NULL DEFAULT 'sencilla',
  `estado` varchar(10) NOT NULL DEFAULT 'libre'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `habitaciones`
--

INSERT INTO `habitaciones` (`numero`, `tipo`, `estado`) VALUES
(101, 'sencilla', 'libre'),
(102, 'sencilla', 'libre'),
(201, 'sencilla', 'libre'),
(202, 'sencilla', 'libre'),
(302, 'doble', 'ocupado'),
(303, 'sencilla', 'libre');

-- --------------------------------------------------------

--
-- Table structure for table `huespedes_actuales`
--

CREATE TABLE IF NOT EXISTS `huespedes_actuales` (
  `id` int(11) NOT NULL,
  `huesped` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `habitacion` int(11) NOT NULL,
  `fecha_entrada` date DEFAULT NULL,
  `fecha_salida` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `huespedes_actuales`
--

INSERT INTO `huespedes_actuales` (`id`, `huesped`, `habitacion`, `fecha_entrada`, `fecha_salida`) VALUES
(13, 'Jorge Carlos Diaz Gongora', 302, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `huespedes_registrados`
--

CREATE TABLE IF NOT EXISTS `huespedes_registrados` (
  `nombre_completo` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `rfc` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `correo` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `telefono` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `huespedes_registrados`
--

INSERT INTO `huespedes_registrados` (`nombre_completo`, `rfc`, `correo`, `telefono`) VALUES
('Cliente Nuevo', 'HYFNP45', 'CLIENTE@hotmail.com', '4423200021'),
('Jorge Carlos Diaz Gongora', 'DIG930710HYN', 'carlosdiazgongora@gmail.com', '4423203111'),
('Juan', '', '', ''),
('KKaa', '', '', ''),
('Manuel', '', '', ''),
('Nueva Persona', '44aq25', '', ''),
('Nuevo Uusario', '', '', ''),
('Pedo Manc', '', '', ''),
('Persona', '', '', ''),
('Persona Nueva', '', '', ''),
('Saul Casal', '', '', ''),
('Usuario doce', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`username`, `password`) VALUES
('usuario', 'password');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `habitaciones`
--
ALTER TABLE `habitaciones`
  ADD PRIMARY KEY (`numero`);

--
-- Indexes for table `huespedes_actuales`
--
ALTER TABLE `huespedes_actuales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `huesped` (`huesped`),
  ADD KEY `habitacion` (`habitacion`);

--
-- Indexes for table `huespedes_registrados`
--
ALTER TABLE `huespedes_registrados`
  ADD PRIMARY KEY (`nombre_completo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `huespedes_actuales`
--
ALTER TABLE `huespedes_actuales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `huespedes_actuales`
--
ALTER TABLE `huespedes_actuales`
  ADD CONSTRAINT `fk_habitaciones` FOREIGN KEY (`habitacion`) REFERENCES `habitaciones` (`numero`),
  ADD CONSTRAINT `fk_hactuales` FOREIGN KEY (`huesped`) REFERENCES `huespedes_registrados` (`nombre_completo`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
