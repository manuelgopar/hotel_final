<?php

//Connection is handled at the very beginning from this instruction
include 'database_connecter.php';
session_start();

//Controller used for the validations.Must be invoked every other php file

//Variables for the 'huespedes_registrados' table
// hr = huesped_registrado
$hr_name        = "";
$hr_email       = "";
$hr_rfc         = "";
$hr_cellphone   = "";


//VARIABLES USED FOR THE FORM ERRORS
$nameErr        = "";
$rfcErr         = "";
$emailErr       = "";
$cellphoneErr   = "";


$_DATA["correct"]= 0;
$validName=0;
$validRFC=0;
$validEmail=0;
$validPhone=0;

// prepare and bind
    $stmt = $conn->prepare("INSERT INTO huespedes_registrados (nombre_completo, correo, rfc, telefono) VALUES (?, ?, ?, ?)");
    $stmt->bind_param("ssss", $hr_name, $hr_email, $hr_rfc, $hr_cellphone);
//Values have been binded


      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["name"])) {
            $nameErr= "Campo Obligatorio";
            $validName=0;

        } else {
            $hr_name = test_input($_POST["name"]); 
            if (!preg_match("/^[a-zA-Z ]*$/",$hr_name)) {
                $nameErr = "Caracteres Inválidos";
                $validName=0;

            }else{
                $validName=1;

            }
        }
           
        if (empty($_POST["rfc"])) {
            $rfcErr= "";
            $hr_rfc = "";
            $validRFC=1;
        } else {
            $hr_rfc = test_input($_POST["rfc"]); 
            if (!preg_match("/^[a-zA-Z0-9 ]*$/",$hr_rfc)) {
                $rfcErr = "Caracteres Inválidos"; 
                $validRFC=0;
            }else {
              $validRFC=1;
            }
        }
          
        if (empty($_POST["email"])) {
            $emailErr= "";
            $hr_email = "";
            $validEmail=1;
        } else {
            $hr_email  = test_input($_POST["email"]);
            if (!filter_var($hr_email, FILTER_VALIDATE_EMAIL)) {
                $emailErr = "Email no válido";
                $validEmail=0;
            }else{
                $validEmail=1;
            } 
        }
        
        if (empty($_POST["phone"])) {
            $cellphoneErr= "";
            $hr_cellphone = "";
            $validPhone=1;
        } else {
            $hr_cellphone = test_input($_POST["phone"]); 
            if (!preg_match("/^[0-9 ]*$/",$hr_cellphone)) {
                $cellphoneErr = "Solo se permiten numeros"; 
                $validPhone=0;
            }else {
               $validPhone=1;
            }
        }
        
    }




    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;    
    }    

    
    if ($validName ==1 && $validRFC ==1 && $validEmail ==1 && $validPhone ==1) {
        $stmt->execute();

        //armamos variable de sesion para el usuario registrado
        //esta seccion es para regresar todas las habitaciones a assignroom.php"
        //todo dentro de la variable $_SESSION se envia magicamente a otro php.
        $_SESSION['user_logged'] = $hr_name;

        $sql = "Select * from habitaciones where estado='libre'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $json[] = $row;
                //echo "id: " . $row["numero"]. " - Tipo: " . $row["tipo"]. " " . $row["estado"]. "<br>";
            }
            $_SESSION['result'] = json_encode($json);
        } else {
            $_SESSION['result'] = [];
        }

        //fin de la seccion

        $nameErr        = "";
        $rfcErr         = "";
        $emailErr       = "";
        $cellphoneErr   = "";
        $validName=0;
        $validRFC=0;
        $validEmail=0;
        $validPhone=0;
        header('location: assignroom_view.php');
    }

    $stmt->close();
    $conn->close();


?>