<?php
	include 'assignroom_controller.php';
	       //creamos la sesion


?>


<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE HTML>
<html>
<head>
<title>Hotel Rey David</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="js/autocomplete.js"></script>
<link href="css/nav.css" rel="stylesheet" type="text/css" media="all"/>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300:700' rel='stylesheet' type='text/css'>
<script src="js/jquery.min.js" type="text/javascript"></script>
<script>
		$(function() {
	    var button = $('#loginButton');
	    var box = $('#loginBox');
	    var form = $('#loginForm');
	    button.removeAttr('href');
	    button.mouseup(function(login) {
	        box.toggle();
	        button.toggleClass('active');
	    });
	    form.mouseup(function() { 
	        return false;
	    });
	    $(this).mouseup(function(login) {
	        if(!($(login.target).parent('#loginButton').length > 0)) {
	            button.removeClass('active');
	            box.hide();
	        }
	    });
	});
   </script>
   <!----font-Awesome----->
<link rel="stylesheet" href="fonts/css/font-awesome.min.css">
<!----font-Awesome----->
<!---strat-date-piker---->
<link rel="stylesheet" href="css/jquery-ui.css" />
<script src="js/jquery-ui.js"></script>
		  <script>
				  $(function() {
				    $( "#datepicker,#datepicker1" ).datepicker();
				  });
		  </script>
<!---/End-date-piker---->
</head>
<body>
   <!-- Header -->
	<div class="header">    
		<div class="header_top">
			<div class="container">

		    <div class="header-top-right">
		        <div class="login_box">
	    		   <div id="loginContainer">
	                  <a id="loginButton" class="active"><span class="active"><i></i>Login Admin</span></a>               
	              </div>
	           </div> 
			 </div>
			 <div class="clearfix"></div>
            </div>
		  </div>
 	    <div class="header_bottom">
		 	  <div class="container">	 			
				<div class="logo">
					<h1><a href="index.html">Hotel<span>Rey David</span></a></h1>
				</div>				
       <div class="clearfix"></div>		   
      </div>
     </div>	
	 <div class="reservation_banner">
		<div class="main_title">Sistema de recepción</div>
		<div class="divider"></div>
	 </div>
	</div>
    
   <!-- Ends Header -->
    <!------------ Start Content ---------------->
       <div class="main"> 
         <div class="reservation_top">     	
          

          <div class="container">
        <?php 
         if(empty($_SESSION['myusername'])){
         ?> 
            <script>
            window.alert('Se debe dar de alta un usuario o elegir uno existente. Se redirigirá al registro')
            window.location.href='registro.php';
            
            </script>    
        <?php
         }else{
         ?>
          <h2>Cliente: <?php echo $_SESSION['myusername'];?></h2>
              <br><br>
          <?php
          if (empty($_SESSION['result'] != 0))
           {
           	?>
           		<div class="container">
                    <div class="reservation_grid">
           		<form action="" method="POST">
                    <h3>Seleccione una de las habitaciones disponibles</h3>
                    <br>
           		<?php if(isset($errorcheckbox))
           			echo "<p>".$errorcheckbox."</p>";
           		?>           		
	                <table class= 'table'>
	                <tr>
	                	<th>Numero</th>
	                	<th>Tipo</th>
	                	<th>Opcion</th>
	                </tr>
                	<?php
                		$items = json_decode($_SESSION['result'], true);
                		foreach($items as $item)
                		{
                			echo "<tr>";
                				echo "<td>".$item['numero']."</td>";
                				echo "<td>".$item['tipo']."</td>";
                				echo "<td><input type='radio' name='option' value='".$item['numero']."'></td>";
                			echo "</tr>";
                		}
                	?>
	                </table>
                 <div class="span2_of_1">
	 		    	<h4>Select check-out date</h4>
					<div class="book_date btm">
							<input class="date" id="datepicker1" type="text" value="DD/MM/YY" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'DD/MM/YY';}">
					</div>	
				 </div>
                 <div class="span2_of_1">
					<h4>Select check-out date</h4>
					<div class="book_date btm">
							<input class="date" id="datepicker1" type="text" value="DD/MM/YY" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'DD/MM/YY';}">
					</div>	
				 </div>
                 <div class="clearfix"></div><br>
	             <input type="submit" class="btn btn-info" name="enviar" value="Asignar habitación" />
                 <input type="submit" class="btn btn-danger" name="cancelar" value="Cancelar"/>
	            </form>
	            </div>
                </div>
	        <?php
           }else
           {
           		echo "valio queso";
           }
           ?>
            <?php
              }  
            ?>
             </div>
            <br><br>
          </div>
        </div>
           
   
        <div class="footer">
         <div class="container">   	 
           	 <div class="footer_top">
           	 	<div class="row">
           	 	   <div class="col-md-4 footer_grid">
           	 			<h4>Receive our Newsletter</h4>
           	 			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
           	 			<div class="search">
						  <form>
							   <input type="text" value="">
							   <input type="submit" value="">
						  </form>
					    </div>
           	 		</div>
           	 		<div class="col-md-4 footer_grid">
           	 			<h4>Twitter Feed</h4>
           	 			<div class="footer-list">
						 <ul>
							<li class="list_top"><i class="fa fa-twitter twt"></i>
							<p>Lorem ipsum <span class="yellow"><a href="#">consectetuer</a></span>vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatu</li></p>
							<li><i class="fa fa-twitter twt"></i>
							<p>Lorem ipsum <span class="yellow"><a href="#">consectetuer</a></span>vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatu</li></p>
		                 </ul>
					    </div>
           	 		</div>
           	 		<div class="col-md-4 footer_grid">
           	 			<h4>Our Address</h4>
           	 			<div class="company_address">
				     	        <p>500 Lorem Ipsum Dolor Sit,</p>
						   		<p>22-56-2-9 Sit Amet, Lorem,</p>
						   		<p>USA</p>
				   		<p>Phone:(00) 222 666 444</p>
				   		<p>Fax: (000) 000 00 00 0</p>
				 	 	<p>Email: <span><a href="mailto:info@mycompany.com">info(at)mycompany.com</a></span></p>
				   		</div>
				      <ul class="socials">
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                      </ul>
           	 		</div>
           	     </div>
           	  </div>
                <div class="footer_bottom">
		           <div class="copy_right">
						<p>&copy; 2013 All Rights Reseverd Designed by <a href="http://w3layouts.com/">W3layouts</a> </p>
				   </div>
				   <div class="footer_nav">
				   	 <ul>
				   	 	<li><a href="index.html">Home</a></li>
				   	 	<li><a href="#">Terms of use</a></li>
				   	 	<li><a href="#">Privacy Policy</a></li>
				   	 	<li><a href="contact.html">Contact</a></li>
				   	 </ul>
				    </div>
				  <div class="clearfix"></div>
				</div>
		   </div>
	</div>
    <script type="text/javascript" src="js/autocomplete.js"></script>
    <script type="text/javascript" src="js/customscript.js"></script>
</body>
</html>
