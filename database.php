<?php

// We will use PDO to execute database stuff. 
// This will return the connection to the database and set the parameter
// to tell PDO to raise errors when something bad happens
function getDbConnection() {
  $db = new PDO(DB_DRIVER . ":dbname=" . DB_DATABASE . ";host=" . DB_SERVER, DB_USER, DB_PASSWORD);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
  return $db;
}

function getSqliDbConnection(){
    
    $db = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);
    if ($db->connect_error) {
        die("Connection failed: " . $db->connect_error);
    }
    return $db;
}


// This is the 'search' function that will return all possible rows starting with the keyword sent by the user
function searchForKeyword($keyword) {
    $db = getDbConnection();
    $stmt = $db->prepare("SELECT nombre_completo as nombre FROM huespedes_registrados WHERE nombre_completo LIKE ? ORDER BY nombre");

    $keyword = $keyword . '%';
    $stmt->bindParam(1, $keyword, PDO::PARAM_STR, 100);

    $isQueryOk = $stmt->execute();
  
    $results = array();
    
    if ($isQueryOk) {
      $results = $stmt->fetchAll(PDO::FETCH_COLUMN);
    } else {
      trigger_error('Error executing statement.', E_USER_ERROR);
    }

    $db = null; 

    return $results;
}

function obtainClientInfo($name){
    $db = getDbConnection();

    $stmt1   = $db->prepare("SELECT rfc , correo, telefono FROM huespedes_registrados 
                            WHERE nombre_completo = ?");

    
    
    $stmt1->bindParam(1, $name, PDO::PARAM_STR, 100);
    $isQueryOk = $stmt1->execute();
    
    $stmt1->execute();
    
    $results= array ();
    
    
    if ($isQueryOk) {
      $results = $stmt1->fetch(PDO::FETCH_ASSOC);
    } else {
      trigger_error('Error executing statement.', E_USER_ERROR);
    }

    $db = null; 
    return $results;
    
}

function searchOccupiedRooms(){
    
    $db = getDbConnection();
    $stmt = $db->prepare("SELECT habitacion FROM huespedes_actuales");
    $isQueryOk = $stmt->execute();
    $results = array();
    if ($isQueryOk) {
      $results = $stmt->fetchALL(PDO::FETCH_COLUMN);
    } else {
      trigger_error('Error executing statement.', E_USER_ERROR);
    }
    $db = null; 
    return $results;
}

function searchCheckoutData($number){
    
    $db = getDbConnection();
    $stmt = $db->prepare("SELECT huesped FROM huespedes_actuales WHERE habitacion = ?");
    $stmt->bindParam(1, $number, PDO::PARAM_INT, 100);
    $isQueryOk = $stmt->execute();
    $results = array();
    if ($isQueryOk) {
      $results = $stmt->fetch(PDO::FETCH_ASSOC);
    } else {
      trigger_error('Error executing statement.', E_USER_ERROR);
    }
    $db = null; 
    return $results;
    
}

?>