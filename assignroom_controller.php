<?php

//Connection is handled at the very beginning from this instruction
include 'database_connecter.php';
session_start();

$errcheckbox = "";

$huesped = "";
$habitacion = "";
$fecha_entrada = "";
$fecha_salida = "";

 if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($_POST['enviar']){
        if(empty($_POST['option']))
            $errorcheckbox = "Debe seleccionar un cuarto!";
        else
        {
            $stmt = $conn->prepare("INSERT INTO huespedes_actuales (huesped, habitacion, fecha_entrada, fecha_salida) VALUES (?, ?, ?, ?)");
            $stmt->bind_param("ssss", $huesped, $habitacion, $fecha_entrada, $fecha_salida);

            $huesped = $_SESSION['user_logged'];
            $habitacion = $_POST['option'];

            $stmt->execute();

            $stmt = $conn->prepare("UPDATE habitaciones SET estado='ocupado' WHERE numero= ?");
            $stmt->bind_param("s",$habitacion);
            $stmt->execute();

            session_destroy();

            header('location: estadoactualhotel_view.php');
            echo "yus!";
        }
    }
    else if ($_POST['cancelar']){
           session_destroy();
           header('location: registro_view.php');
    }
 }

?>