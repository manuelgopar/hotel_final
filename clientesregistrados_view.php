<?php
    include 'clientesregistrados_controller.php';
       //creamos la sesion

?>



<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE HTML>
<html>
<head>
<title>Hotel Rey David</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="js/autocomplete.js"></script>
<link href="css/nav.css" rel="stylesheet" type="text/css" media="all"/>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300:700' rel='stylesheet' type='text/css'>
<script src="js/jquery.min.js" type="text/javascript"></script>
<script>
		$(function() {
	    var button = $('#loginButton');
	    var box = $('#loginBox');
	    var form = $('#loginForm');
	    button.removeAttr('href');
	    button.mouseup(function(login) {
	        box.toggle();
	        button.toggleClass('active');
	    });
	    form.mouseup(function() { 
	        return false;
	    });
	    $(this).mouseup(function(login) {
	        if(!($(login.target).parent('#loginButton').length > 0)) {
	            button.removeClass('active');
	            box.hide();
	        }
	    });
	});
   </script>
   <!----font-Awesome----->
<link rel="stylesheet" href="fonts/css/font-awesome.min.css">
<!----font-Awesome----->
<!---strat-date-piker---->
<link rel="stylesheet" href="css/jquery-ui.css" />
<script src="js/jquery-ui.js"></script>
		  <script>
				  $(function() {
				    $( "#datepicker,#datepicker1" ).datepicker();
				  });
		  </script>
<!---/End-date-piker---->
</head>
<body>
   <!-- Header -->
	<div class="header">    
		<div class="header_top">
			<div class="container">

		    <div class="header-top-right">
		        <div class="login_box">
	    		   <div id="loginContainer">
              <a href="logout.php"><span><i></i>Logout Admin</span></a>  
	              </div>
	           </div> 
			 </div>
			 <div class="clearfix"></div>
            </div>
		  </div>
 	    <div class="header_bottom">
		 	  <div class="container">	 			
				<div class="logo">
					<h1><a href="index.html">Hotel<span>Rey David</span></a></h1>
				</div>				
    <div class="navigation">	
        <div>
          <label class="mobile_menu" for="mobile_menu">
          <span>Menu</span>
          </label>
          <input id="mobile_menu" type="checkbox">
            <ul class="nav">
                 <li><a href="registro_view.php">Registro de clientes</a></li>                  
                 <li><a href="#">Clientes Registrados</a></li> 
                 <li><a href="estadoactualhotel_view.php">Estado actual</a></li>     
                 <li><a href="checkout_view.php">Checkout</a></li>            
            <div class="clearfix"></div>
            </ul>
		</div>				   
      </div>
     </div>	
	 <div class="reservation_banner">
		<div class="main_title">Sistema de recepción</div>
		<div class="divider"></div>
	 </div>
	</div>
    
   <!-- Ends Header -->
    <!------------ Start Content ---------------->
       <div class="main"> 
         <div class="reservation_top">     	
            
          <div class="container">                     
           <!--Forma para relacionada a la información del huesped-->
          <form id="clientesExistenteForm" method="POST" action="">       
		   <div class="personal_bottom">
		   	 <h2 class="head">Busqueda de huesped existente</h2>
			     <div class="reservation-form">
                     <div class="span2_of_1">
                        <div>
                          <span><label>Nombre completo del cliente a buscar</label></span>
                            <p id="nombreErr"></p>
                          <input type="text" value="" class="textbox" id="keyword" autocomplete="off" name="regUser">
                        <div id="results">
                        </div>  
                        </div>
                         <br><br><br><br>
                     </div>
                     <div class="span2_of_1" id="selectedClientInformation" hidden="">
                        <div>
                          <span><label>RFC</label></span>
                          <input type="text" class="textbox" name="rfc" id="rfc">
                        </div> 
                        <div>
                          <span><label>Email</label></span>
                          <input type="text" class="textbox" name="email" id="email">
                        </div>
                        <div>
                          <span><label>Phone</label></span>
                          <input type="text" class="textbox" name="phone" id="phone">
                        </div>
                     </div>
                     <div class="clearfix"></div>
                     <div hidden="" id="assignButton">
                        <input class="btn btn-info" title="book now" type="submit" name="Enviar" id="Enviar" value="Asignar habitación">
                        <input id="clear" type="button" class="btn" value="Buscar de nuevo" onclick="clearInfo();" />
                     </div>
                     
                      <!--<a href="#" title="book now" class="btn btn-primary btn1 btn-normal btn-inline " target="_self">Ingresar usuario</a>-->
                    </div>
                </div>
            </form>
           </div>
          </div>
        </div>
           
   
        <div class="footer">
         <div class="container">   	 
           	 <div class="footer_top">
           	 	<div class="row">
           	 	   <div class="col-md-4 footer_grid">
           	 			<h4>Receive our Newsletter</h4>
           	 			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
           	 			<div class="search">
						  <form>
							   <input type="text" value="">
							   <input type="submit" value="">
						  </form>
					    </div>
           	 		</div>
           	 		<div class="col-md-4 footer_grid">
           	 			<h4>Twitter Feed</h4>
           	 			<div class="footer-list">
						 <ul>
							<li class="list_top"><i class="fa fa-twitter twt"></i>
							<p>Lorem ipsum <span class="yellow"><a href="#">consectetuer</a></span>vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatu</li></p>
							<li><i class="fa fa-twitter twt"></i>
							<p>Lorem ipsum <span class="yellow"><a href="#">consectetuer</a></span>vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatu</li></p>
		                 </ul>
					    </div>
           	 		</div>
           	 		<div class="col-md-4 footer_grid">
           	 			<h4>Our Address</h4>
           	 			<div class="company_address">
				     	        <p>500 Lorem Ipsum Dolor Sit,</p>
						   		<p>22-56-2-9 Sit Amet, Lorem,</p>
						   		<p>USA</p>
				   		<p>Phone:(00) 222 666 444</p>
				   		<p>Fax: (000) 000 00 00 0</p>
				 	 	<p>Email: <span><a href="mailto:info@mycompany.com">info(at)mycompany.com</a></span></p>
				   		</div>
				      <ul class="socials">
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                      </ul>
           	 		</div>
           	     </div>
           	  </div>
                <div class="footer_bottom">
		           <div class="copy_right">
						<p>&copy; 2013 All Rights Reseverd Designed by <a href="http://w3layouts.com/">W3layouts</a> </p>
				   </div>
				   <div class="footer_nav">
				   	 <ul>
				   	 	<li><a href="index.html">Home</a></li>
				   	 	<li><a href="#">Terms of use</a></li>
				   	 	<li><a href="#">Privacy Policy</a></li>
				   	 	<li><a href="contact.html">Contact</a></li>
				   	 </ul>
				    </div>
				  <div class="clearfix"></div>
				</div>
		   </div>
	</div>
    <script type="text/javascript" src="js/autocomplete.js"></script>
    <script type="text/javascript" src="js/customscript.js"></script>
</body>
</html>
