<?php

require('constant.php');
require('database.php');

if (!isset($_GET['keyword'])) {
	die();
}else{
    $keyword = $_GET['keyword'];
    $data = searchForKeyword($keyword);
    echo json_encode($data);
}


?>